class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :name
      t.string :phone_number

      t.timestamps
    end
  end
end
