class CreateLisitings < ActiveRecord::Migration
  def change
    create_table :lisitings do |t|
      t.string :name
      t.string :phone_number

      t.timestamps
    end
  end
end
