Loader = (function(){
  var default_ns = PhoneBook;

  return {
    exec: function(controller, action) {
      var ns = default_ns;
      action = (action === undefined) ? "init" : action;

      if(controller !== "" && ns[controller] && typeof ns[controller][action] == "function") {
        ns[controller][action]();
      }
    },

    init: function() {
      var body = document.body,
          controller = body.getAttribute( "data-controller" ),
          action = body.getAttribute( "data-action" );

      Loader.exec( "common" );
      Loader.exec(controller );
      Loader.exec(controller, action);
    }
  };

})();

$(document).ready(Loader.init);